
import { SceneController } from "../src/scripts/scenes/sceneController"

export const numberOfTasks: number = 3;

export default class Game extends Phaser.Game {

    private resize() {
        const canvas = this.canvas;
        if (canvas) {
            const w = window.innerWidth;
            const h = window.innerHeight;
            const scale = Math.min(w / this.BaseWidth, h / this.BaseHeight);
    
            canvas.setAttribute("style",
                " -ms-transform: scale(" + scale + "); -webkit-transform: scale3d(" + scale + ", 1);" +
                " -moz-transform: scale(" + scale + "); -o-transform: scale(" + scale + "); transform: scale(" + scale + ");" +
                " transform-origin: top left;",
            );
    
            const width = w / scale;
            const height = h / scale;
            this.scale.resize(width, height);
            this.events.emit('event-resize');
        }
    }
    public get BaseWidth() {
        return Number(this.config.width);
    }
    public get BaseHeight() {
        return Number(this.config.height);
    }
    public get BaseRatio() {
        return this.BaseWidth / this.BaseHeight;
    }
    constructor(i_GameConfig: Phaser.Types.Core.GameConfig) {
        super(i_GameConfig);
    
        this.resize();
        window.addEventListener("resize", () => this.resize());
        new SceneController(this);
    }

}