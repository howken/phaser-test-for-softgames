
const originX = 0;
const originY = 0.5;
const originalEmojiSize = 256;

const texts = [
    "GARDEN",
    "TALES",
    "YEAH",
    "AWESOME",
    "GET 'EM",
    "WOOHOO",
    "FUN"
];
const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!!';

const minSizePopupElement = 30;
const maxSizePopupElement = 70;

const minLengthRandomText = 3;
const maxLengthRandomText = 6;

const xOffsetBetweenObjects = 10;

export function getRandomBetween(from: number, to: number): number {
    if(from >= to) {
        let temp = from;
        from = to;
        to = temp;
    }
    return Math.random() * (to - from) + from;
}

export class RandomDynamicTextPopup {

    private popupElements: (Phaser.GameObjects.Text | Phaser.GameObjects.Image)[];
    private numberOfElements: number;
    private size: number;
    private width: number;

    constructor(
        protected scene: Phaser.Scene,
        private x: number,
        private y: number,
        minNumberOfElements: number,
        maxNumberOfElements: number,
        duration: number
    ) {
        this.numberOfElements = getRandomBetween(minNumberOfElements, maxNumberOfElements);
        this.popupElements = [];
        this.startPopup(x, y, duration);
    }

    protected startPopup(x: number, y: number, duration: number = 2000): void {

        this.size = getRandomBetween(minSizePopupElement, maxSizePopupElement);
        const scale = this.size / originalEmojiSize;
        this.width = 0;

        for(let i = 0; i < this.numberOfElements; i++) {
            let currentElement;

            // initially, the x position of the 0th element will be x
            let xx = x;
            if(i > 0) {
                // all subsequent elements go to the right side of the previous element
                xx = this.popupElements[i - 1].x + this.popupElements[i - 1].displayWidth + xOffsetBetweenObjects;
                this.width += xOffsetBetweenObjects;
            }

            const randomBool = Math.round(Math.random());

            // make a text
            if(randomBool == 0) {
                const textLength = getRandomBetween(minLengthRandomText, maxLengthRandomText);
                const style = {
                    fontSize: this.size,
                    color: '#000',
                };

                const randomText = Math.round(Math.random()) == 0 ?
                    this.generateRandomText(textLength) :
                    this.generateActualText(texts);
                
                currentElement = this.scene.add.text(0, 0, randomText, style);
            }
            // make an image
            else {
                currentElement = this.scene.add.image(0, 0, 'emoji');

                currentElement.setScale(scale);
                const frames = currentElement.texture.getFrameNames().length;
                currentElement.setFrame(`${Math.floor(Math.random() * frames)}.png`);
            }
            this.popupElements.push(currentElement);

            this.width += this.popupElements[i].displayWidth;

            this.popupElements[i].setPosition(xx, y)
            .setOrigin(originX, originY)
            .setAlpha(0);
        }

        console.log(this.popupElements);
        this.setPosition();
        this.animatePopup(duration);
    }

    protected animatePopup(popupDuration: number): void {

        this.scene.add.tween({
            targets: this.popupElements,
            duration: popupDuration * 0.2,
            alpha: 1,
            ease: "Linear"
        });

        const startedFadout = false;
        const showingTween = this.scene.add.tween({
            targets: this.popupElements,
            duration: popupDuration,
            y: this.y * 0.9,
            ease: "Linear",
            onUpdate: () => {
                if(!startedFadout && 
                    showingTween.elapsed >= popupDuration * 0.9
                ) {
                    this.scene.add.tween({
                        targets: this.popupElements,
                        duration: popupDuration - showingTween.elapsed,
                        alpha: 0,
                        ease: "Linear"
                    });
                }
            }
        });
    }

    protected setPosition(): void {
        // centers the entire popup in the middle of the screen
        this.popupElements.forEach(element => {
            const originalX = element.x;
            element.setX(originalX - this.width / 2);
        });
    }

    protected generateRandomText(length: number): string {

        let text = "";
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
           text += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return text;
    }

    protected generateActualText(arrayOfStrings: string[]): string {
        return arrayOfStrings[Math.round(Math.random() * arrayOfStrings.length)];
    }

}