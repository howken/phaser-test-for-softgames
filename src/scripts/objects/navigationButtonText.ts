import { GameSceneKey } from "../scenes/sceneController";

export default class NavButtonText extends Phaser.GameObjects.Text {
    constructor(
        protected scene: Phaser.Scene, 
        protected gameSceneKey: GameSceneKey,
        text: string,
        style: Phaser.Types.GameObjects.Text.TextStyle, 
        protected styleHover: Phaser.Types.GameObjects.Text.TextStyle
    ) {
        super(scene, 0, 0, text, style);

        scene.add.existing(this);

        this.on('pointerup', this.onClick, this);
        this.on('pointerover', () => {
            this.setStyle(styleHover);
        }, this);
        this.on('pointerout', () => {
            this.setStyle(style);
        }, this);

        this.setInteractive();
    }

    protected onClick(): void {
        this.scene.game.events.emit('event-load-scene', this.gameSceneKey);
    }
}