import 'phaser'
import Game from '../Game'
import PreloadScene from './scenes/preloadScene';
import UIScene from './scenes/UIScene';
import MainMenuScene from './scenes/demoTestScenes/mainMenuScene';
import SpriteStacksScene from './scenes/demoTestScenes/spriteStacksScene';
import TextPopupsScene from './scenes/demoTestScenes/textPopupsScene';
import FireScene from './scenes/demoTestScenes/fireScene';

const config: Phaser.Types.Core.GameConfig = {
  type: Phaser.AUTO,
  scale: {
    mode: Phaser.Scale.NONE,
    parent: "game", 
    width: 800,
    height: 600
},
  scene: [
    PreloadScene,
    MainMenuScene,
    SpriteStacksScene,
    TextPopupsScene,
    FireScene,
    UIScene
  ]
}

window.addEventListener('load', () => {
  const game = new Game(config);
})
