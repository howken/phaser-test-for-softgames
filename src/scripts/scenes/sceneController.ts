
export const enum GameSceneKey {
    MainMenu = "MainMenu",
    SpriteStacks = "SpriteStacks",
    TextPopups = "TextPopups",
    Fire = "Fire"
}

export function getGameSceneKeyByIndex(index?: number): GameSceneKey {
    switch(index) {
        case 0: return GameSceneKey.SpriteStacks
        case 1: return GameSceneKey.TextPopups
        case 2: return GameSceneKey.Fire
        default: return GameSceneKey.MainMenu
    }
}
export function getGameSceneLabelByIndex(index?: number): string {
    switch(index) {
        case 0: return "Sprite Stacks"
        case 1: return "Dynamic Popups"
        case 2: return "Fire"
        default: return "Main Menu"
    }
}

export class SceneController {
    private currentSceneKey: string;
    private scenes: Phaser.Scene[];

    constructor(private game: Phaser.Game) {
        this.scenes = [];
        for(let i = -1; i < 3; i++) {
            this.scenes.push(this.game.scene.getScene(getGameSceneKeyByIndex(i)))
        }
        this.init();
    }

    private init(): void {
        this.game.events.on('event-load-scene', this.startScene, this);
        this.startScene(GameSceneKey.MainMenu);
    }

    private startScene(scene: GameSceneKey): void {
        console.log('Switch Scene ', scene, ' From ', this.currentSceneKey);

        if(!this.game.scene.getScene(scene)) {
            this.game.scene.start(scene);
        }
        else {
            this.game.scene.switch(this.currentSceneKey, scene);
            this.game.scene.stop(this.currentSceneKey);
        }
        this.currentSceneKey = scene;
    }
  }
  