export default class PreloadScene extends Phaser.Scene {
  constructor() {
    super({ key: 'PreloadScene' })
  }

  preload() {
    this.load.atlas('pokemon', 'assets/img/pokemon.png', 'assets/data/pokemon.json');
    this.load.atlas('emoji', 'assets/img/emoji.png', 'assets/data/emoji.json');
    this.load.image('fire1', 'assets/img/fire.png');
  }

  create() {
    this.scene.start('UI');
    this.scene.start('MainMenu');
  }
}
