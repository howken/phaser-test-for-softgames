import { getGameSceneKeyByIndex, getGameSceneLabelByIndex } from "../sceneController";
import { numberOfTasks } from "../../../Game";
import NavButtonText from "../../objects/navigationButtonText";
import DemoTestScene from "./demoTestScene";
import { defaultStyleMenuButton, defaultStyleMenuButtonHover } from "../UIScene";

export default class MainMenuScene extends DemoTestScene {

  private readonly menuButtonsPositionCfg = {
    yAbsoluteOffsetFactor: 0.15,
    yOffsetFactor: 0.2
  }
  
  private title: Phaser.GameObjects.Text;
  private navButtons: NavButtonText[];

  constructor() {
    super({ key: 'MainMenu' });
    this.backgroundColor = "#fff";
  }

  createSpecific(): void {
    this.navButtons = [];

    for (let i = 0; i < numberOfTasks; i++) {
      const current = new NavButtonText(
        this,
        getGameSceneKeyByIndex(i),
        getGameSceneLabelByIndex(i),
        defaultStyleMenuButton,
        defaultStyleMenuButtonHover
      );
      current.setOrigin(0.5, 0.5);
      this.navButtons.push(current);
    }

    this.title = this.add.text(0, 0, 'Thank You!')
    .setFontSize(100)
    .setStyle({
        fontSize: '50px',
        fontFamily: 'Arial',
        rtl: false,
        color: '#f00',
        stroke: '#fff',
        strokeThickness: 5
    })
    .setOrigin(0.5, 0.5);


    this.resize();
  }

  resize(): void {
    for(let i = 0; i < this.navButtons.length; i++) {
      let y = this.game.scale.gameSize.height *
      (this.menuButtonsPositionCfg.yAbsoluteOffsetFactor + this.menuButtonsPositionCfg.yOffsetFactor * i);

      this.navButtons[i].setPosition(this.game.scale.gameSize.width * 0.5, y);
    }

    this.title.setPosition(
      this.game.scale.gameSize.width * 0.5,
      this.game.scale.gameSize.height *
      (this.menuButtonsPositionCfg.yAbsoluteOffsetFactor + this.menuButtonsPositionCfg.yOffsetFactor * this.navButtons.length)
    );
  }
}
