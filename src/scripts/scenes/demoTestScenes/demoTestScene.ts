
export default abstract class DemoTestScene extends Phaser.Scene {
    protected backgroundColor: string;
    
    constructor(config: string | Phaser.Types.Scenes.SettingsConfig) {
        super(config);
    }

    create(): void {
        this.createSpecific();
        this.game.events.on('event-resize', () => {
            if(this.scene.isActive()) this.resize();
            
        }, this);
        this.cameras.main.setBackgroundColor(this.backgroundColor);
    }

    protected abstract createSpecific(): void;
    protected abstract resize(): void;
}