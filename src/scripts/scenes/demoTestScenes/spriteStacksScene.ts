import DemoTestScene from "./demoTestScene";
import Stack from 'stack-lifo';
import { GameSceneKey } from "../sceneController";

export default class SpriteStacksScene extends DemoTestScene {
  protected readonly requiredSprites = 144;
  
  protected readonly spriteOffsetX = 0.3;
  protected readonly spriteOffsetY = 0.3;
  protected readonly defaultSpriteScale = 2;

  protected readonly initialStackPosition = {
    relativeX: 0.25,
    relativeY: 0.5
  }
  protected readonly targetStackPosition = {
    relativeX: 0.75,
    relativeY: 0.5
  }

  protected iterator: number;

  protected initialStack: Stack<Phaser.GameObjects.Sprite>;
  protected targetStack: Stack<Phaser.GameObjects.Sprite>;

  protected currentCard: Phaser.GameObjects.Sprite;

  constructor() {
    super({ key: 'SpriteStacks' });
    this.backgroundColor = "#fff";
  }

  createSpecific() {
    this.iterator = 0;
    
    this.initialStack = this.initStack(
      this.game.scale.gameSize.width * this.initialStackPosition.relativeX,
      this.game.scale.gameSize.height * this.initialStackPosition.relativeY,
      false,
      'pokemon',
      this.defaultSpriteScale
    );
    this.targetStack = this.initStack(
      this.game.scale.gameSize.width * this.targetStackPosition.relativeX,
      this.game.scale.gameSize.height * this.targetStackPosition.relativeY,
      true,
      'pokemon',
      this.defaultSpriteScale
    );

    if (!this.targetStack || !this.initialStack) {
      console.log('Not enough frames!');
      this.game.events.emit('event-load-scene', GameSceneKey.MainMenu);
      return;
    }

    this.resize();
    this.startTween(this.initialStack, this.targetStack, 2000, 500);
  }

  protected startTween(
    initialStack: Stack<Phaser.GameObjects.Sprite>,
    targetStack: Stack<Phaser.GameObjects.Sprite>,
    durationPerCard: number = 2000,
    delay: number = 0
  ) {
    if(initialStack.size() == 0){
      return;
    }
    setTimeout(() => {
      this.currentCard = initialStack.pop();
      
      const targetPosition = {
        x: this.targetStackPosition.relativeX * this.game.scale.gameSize.width - this.spriteOffsetX * this.iterator,
        y: this.targetStackPosition.relativeY * this.game.scale.gameSize.height - this.spriteOffsetY * this.iterator
      };
      
      let depthChanged = false;
      const showingTween = this.tweens.add({
            targets: this.currentCard,
            duration: durationPerCard,
            x: targetPosition.x,
            y: targetPosition.y,
            ease: 'Linear',
            onUpdate: () => {
              // scales to 2x it's size, and goes back to original size throughout the duration of the tween
              let x = showingTween.elapsed / 1000;
              let scaleFactor = 1 + Math.sin(Math.PI * 0.5 * x);
    
              if(scaleFactor < 1) scaleFactor = 1;
              else if (scaleFactor > 2) scaleFactor = 1;
    
              this.currentCard.setScale(this.defaultSpriteScale * scaleFactor);

              // change depth when the card is in the middle of two stacks
              if(!depthChanged && showingTween.elapsed > durationPerCard * 0.5) {
                this.currentCard.setDepth(this.iterator);
                depthChanged = true;
              }
            },
            onComplete: () => {
              this.iterator++;
              this.startTween(initialStack, targetStack, durationPerCard, delay);
            }
      });
    }, delay);
  }

  protected initStack(
    x:number,
    y: number,
    isEmpty: boolean,
    textureKey: string,
    spriteScale?: number
  ): Stack<Phaser.GameObjects.Sprite> {
    let sprites = new Stack();

    if(!isEmpty) {
      let sprite = this.add.sprite(0, 0, textureKey);
      if(sprite.texture.frameTotal - 1 < this.requiredSprites) {
        return null;
      }
      sprite.destroy();
      
      for(let i = 0; i < sprite.texture.frameTotal; i++) {
          sprite = this.add.sprite(
            x - this.spriteOffsetX * i,
            y - this.spriteOffsetY * i,
            textureKey,
            i
          );
          sprite.setDepth(i);
          spriteScale && sprite.setScale(spriteScale);
          sprites.push(sprite);
      }
    }

    return sprites;
  }

  resize() {}
}
