import DemoTestScene from "./demoTestScene";
import { getRandomBetween, RandomDynamicTextPopup } from "../../objects/randomDynamicTextPopup";

const delayBetweenPopups = 2000;

export default class TextPopupsScene extends DemoTestScene {
  timedEvent;

  constructor() {
    super({ key: 'TextPopups' });
    this.backgroundColor = "#fff";
  }

  createSpecific(): void {
    this.timedEvent = this.time.addEvent({
      delay: delayBetweenPopups,
      callback: () => {
        let x = this.game.scale.gameSize.width * 0.5;
        let y = getRandomBetween(this.game.scale.gameSize.height * 0.4, this.game.scale.gameSize.height * 0.6);
        new RandomDynamicTextPopup(this, x, y, 1, 5, delayBetweenPopups);
      },
      callbackScope: this,
      loop: true,
    });
    this.resize();
  }

  resize() {
  }
 

}
