import DemoTestScene from "./demoTestScene";
import { defaultUITextStyle, textOffsetFromEdges, defaultFontSize } from "../UIScene";

export default class FireScene extends DemoTestScene {
  protected fireEmitterManager: Phaser.GameObjects.Particles.ParticleEmitterManager;
  protected fireEmitter: Phaser.GameObjects.Particles.ParticleEmitter;
  protected particleText: Phaser.GameObjects.Text;

  constructor() {
    super({ key: 'Fire' })
    this.backgroundColor = "#000";
  }

  createSpecific() {
    this.particleText = this.add.text(0, 0, '', defaultUITextStyle)
     .setOrigin(0, 0);

    this.fireEmitterManager = this.add.particles('fire1');
    this.resize();

    this.startFire(this.fireEmitterManager);
  }

  startFire(emitter: Phaser.GameObjects.Particles.ParticleEmitterManager): void {
    this.fireEmitter = emitter.createEmitter({
      alpha: { start: 1, end: 0 },
      scale: { start: 0.5, end: 2.5 },
      tint: { start: 0xffff00, end: 0xff5500 },
      speed: 50,
      accelerationY: -300,
      angle: { min: -65, max: -75 },
      rotate: { min: -90, max: 90 },
      x: { min: -50, max: 50 },
      y: { min: -20, max: 20 },
      lifespan: 1000,
      blendMode: 'ADD',
      frequency: 101,
    });
  }
  
  update() {
    this.particleText.setText("particles: " + this.fireEmitter.getAliveParticleCount());
  }

  resize() {
    this.particleText.setPosition(textOffsetFromEdges, 2 * textOffsetFromEdges + defaultFontSize);
    this.fireEmitterManager.setPosition(this.game.scale.gameSize.width * 0.5, this.game.scale.gameSize.height * 0.65);
  }

}
