import { GameSceneKey, getGameSceneLabelByIndex } from "./sceneController";
import NavButtonText from "../objects/navigationButtonText";

export const textOffsetFromEdges = 15;
export const defaultFontSize = 32;

export const defaultUITextStyle = {
  color: '#000',
  fontSize: defaultFontSize + "px",
  stroke: '#fff',
  strokeThickness: 2,
}

export const defaultStyleMenuButtonHover = {
  fontSize: '85px',
  fontFamily: 'Arial',
  rtl: false,
  color: '#fff',
  stroke: '#000',
  strokeThickness: 5,
};


export const defaultStyleMenuButton = {
  fontSize: '80px',
  fontFamily: 'Arial',
  rtl: false,
  color: '#000',
  stroke: '#fff',
  strokeThickness: 5,
};

export default class UIScene extends Phaser.Scene {
  protected fpsText: Phaser.GameObjects.Text;
  protected phaserVersionText: Phaser.GameObjects.Text;
  protected backToMainMenuText: Phaser.GameObjects.Text;

  constructor() {
    super({ key: 'UI' })
  }

  create() {
    this.fpsText = this.add.text(0, 0, '', defaultUITextStyle)
    .setOrigin(0, 0);

    let styleHover = {
      fontSize: defaultFontSize + 2 + "px",
      fontFamily: 'Arial',
      rtl: false,
      color: '#fff',
      stroke: '#000',
      strokeThickness: 5,
    };

    this.backToMainMenuText = new NavButtonText(
      this,
      GameSceneKey.MainMenu,
      getGameSceneLabelByIndex(),
      defaultUITextStyle,
      styleHover
    );
    
    this.backToMainMenuText
      .setOrigin(0.5, 0)
      .setActive(false)
      .setAlpha(0);
      
    this.phaserVersionText = this.add
      .text(0, 0, `Phaser v${Phaser.VERSION}`, defaultUITextStyle)
      .setOrigin(1, 0);

      this.game.events.on('event-resize', this.resize, this);

      this.backToMainMenuText.on('pointerdown', () => {
        this.game.events.emit('event-load-scene', GameSceneKey.MainMenu);
      }, this);

      this.game.events.on('event-load-scene', (scene) => {
        if(scene == GameSceneKey.MainMenu) {
          this.backToMainMenuText.setActive(false).setAlpha(0);
        }
        else {
          this.backToMainMenuText.setActive(true).setAlpha(1);
        }
       }, this);

      this.resize();
  }

  update() {
      this.fpsText.setText(`fps: ${Math.floor(this.game.loop.actualFps)}`);
  }

  resize() {
    this.fpsText.setPosition(textOffsetFromEdges, textOffsetFromEdges);
    this.phaserVersionText.setPosition(this.game.scale.gameSize.width - textOffsetFromEdges, textOffsetFromEdges);
    this.backToMainMenuText.setPosition(this.game.scale.gameSize.width * 0.5, textOffsetFromEdges);
  }

}
